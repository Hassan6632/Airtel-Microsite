<?php

/*
*   The basename() function returns the filename from a path.
*   $file_extension = pathinfo($file_name, PATHINFO_EXTENSION);
*   
*/

require 'db.php';

$isError = false;
$link_builder = uniqid(rand(), true).round(microtime(true)).mt_rand();

//Check is form submitted 
if(isset($_POST['submit'])){
    
    // Check is file uploaded
    if(is_uploaded_file($_FILES['photo']['tmp_name'])){
        
        //Check has error in file uplaoding
        if($_FILES['photo']['error'] == UPLOAD_ERR_OK){
            
            //Defines allowed image types
            $allowed_extensions =  array('image/gif','image/png' ,'image/jpg', 'image/jpeg');
            $photo_type = $_FILES['photo']['type'];
            
            // Check is image type is allowed
            if(in_array($photo_type,$allowed_extensions) ) {
                
                $photo_name = $link_builder.'.'.pathinfo(basename($_FILES['photo']['name']), PATHINFO_EXTENSION);
                $photo_name_with_path = "upload/".$photo_name;
                
                setcookie('link_builder', $link_builder, time()+60*60*24*30, '/');
                
                // Move uploaded photo to directory
                if(move_uploaded_file($_FILES['photo']['tmp_name'], $photo_name_with_path)){
                    
                    $stmt = $db->prepare('INSERT INTO personal_info (link_builder,image,source) VALUES(:link_builder,:image,:source)');
                    $stmt->bindParam(':link_builder', $link_builder);
                    $stmt->bindParam(':image', $photo_name_with_path);
                    $stmt->bindValue(':source', "Airtel Microsite");
                    $stmt->execute();

                    if($stmt->rowCount()){
                        header("Location: signup.php");
                    }
                }
               
            }
            else{
               $isError = true;
            }


        }else{
             $isError = true;
        }
        
    }else{
         $isError = true;
    }
   
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--    [Site Title] -->
    <title>#airtelishere | Check-in</title>
    <!--    [Bootstrap Css] -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <!--    [Animate Css]-->
    <link rel="stylesheet" href="assets/css/animate.css">
   <!--    [FontAwesome Css] -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <!--    [IcoFont Css] -->
    <link rel="stylesheet" href="assets/css/icofont.css">
    <!--    [OwlCarousel Css]-->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <!--    [Custom Stlesheet]-->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <!--    [Favicon] -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
</head>

<body>
    <!--PRELOADER START-->
    <style>
        .file-input-wrapper >.btn-file-input {
            font-size: 25px;
            color: #fff;
            border-radius: 0;
            margin-top: 20px;
            width: 205px;
            height: 60px;
            background: #ED1C24;
        }
        .onlu-up-btn-hid{
            opacity: 0;
        }
        .onlu-up-btn-show{
            opacity: 1;
        }
        .file-input-wrapper{
            padding-bottom: 0;
            text-align: center;
            margin-bottom: -55px;
        }
        .onlu-up-btn {
            text-align:  center;
        }

        .onlu-up-btn input {
            padding:  10px 25px;
            font-size:  20px;
            background:  #fff;
            border:  1px solid #ED1C24;
            color:  #ED1C24;
            cursor: pointer;
            margin-bottom: 50px;
        }
        
    </style>

    <!--    [ Strat Header Area]-->
    <header>
        <!--    [ Strat Logo Area]-->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="head-design">
                       <img src="assets/img/header.png" alt="">
                   </div>
                </div>
            </div>
        </div>
        <!--    [Finish Logo Area]-->
    </header>
    <!--    [Finish Header Area]-->

    <!--    [ Strat Section Title Area]-->
    <section id="new-check-in">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="airtel-banner">
                        <img src="assets/img/banner04.jpg">
                    </div>
                     <div class="map-pointer">
                        <div class="bd-map">
                            <img src="assets/img/map.png" alt="">
                        </div>
                        <div class="air-pointer">
                            <img src="assets/img/point.png" alt="">
                        </div>
                    </div>
                    
                    <?php if($isError): ?>
                        <div class="alert alert-warning" role="alert">
                         Please take a photo.
                        </div>
                    <?php endif ?>

                    <!-- [FORM START]  -->
                    <form action="index.php" method="post" enctype="multipart/form-data">
                         <div class="only-upload">
                            <div class="up-pic-btn file-input-wrapper">
                                <button class="btn-file-input">Take A Photo</button>
                                 <input  id="tstpic" type="file" name="photo" accept="image/*;capture=camera">
                            </div>
                            <div class="onlu-up-btn">
                                <input type="submit" name="submit" value="Upload">
                            </div>
                        </div>
                    </form>
                   <!-- [FORM END] -->
                   
                </div>
            </div>
        </div>
    </section>
    <!--    [Finish Section Title Area]-->


    <!--    [ Strat Section Area]-->
    <!--    [Finish Section Area]-->


    <!--    [ Start Footer Area]-->
    <footer>
        <div class="copy-right">
            <div class="container">
               <div class="copy-content">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <p>&copy; 2018. All Rights Reserved. Theme By <span>PreneurLAB.com</span></p>
                        </div>
                    </div>
               </div>
            </div>
        </div>
    </footer>
    <!--    [Finish Footer Area]-->

    <!--SCROLL TOP BUTTON-->
    <a href="#" class="top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <!--    [jQuery]-->
    <script src="assets/js/jquery-3.2.1.min.js"></script>

    <!--    [Popper Js] -->
    <script src="assets/js/popper.min.js"></script>

    <!--    [Bootstrap Js] -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!--    [OwlCarousel Js]-->
    <script src="assets/js/owl.carousel.min.js"></script>

    <!--    [Navbar Fixed Js] -->
    <script src="assets/js/navbar-fixed.js"></script>

    <!--    [Main Custom Js] -->
    <script src="assets/js/main.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
   <script>
       $(document).ready(
        function(){
        $('input:submit').hide(),
            $('input:file').change(
                function(){
                    if ($(this).val()) {
                        $('input:submit').show(); 
                    } 
                }
                );
        });
    </script>
    <script>
    </script>

</body>

</html>