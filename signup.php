<?php

require 'db.php';

if(!isset($_COOKIE['link_builder'])){
    header('index.php');
}

if(isset($_POST['submit'])){
    
    $name = htmlspecialchars($_POST['name'], ENT_QUOTES);
    $phone = htmlspecialchars($_POST['phone'], ENT_QUOTES);
    $thana = htmlspecialchars($_POST['thana'], ENT_QUOTES);
    $geo_lat = htmlspecialchars($_POST['geo_lat'], ENT_QUOTES);
    $geo_long = htmlspecialchars($_POST['geo_long'], ENT_QUOTES);
    
    $stmt = $db->prepare('SELECT * FROM thana_info WHERE thana=:thana');
    $stmt->bindParam(':thana', $thana);
    $stmt->execute();
    $thana_lat_lng = $stmt->fetch(PDO::FETCH_ASSOC);
    $link_builder = $_COOKIE['link_builder'];
    
    $stmt = $db->prepare('UPDATE personal_info SET geo_lat=:geo_lat, geo_long=:geo_long,name=:name, phone=:phone, thana=:thana,latitude=:latitude,longitude=:longitude WHERE link_builder =:link_builder ');
    $stmt->bindParam(':geo_lat', $geo_lat);
    $stmt->bindParam(':geo_long', $geo_long);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':phone', $phone);
    $stmt->bindParam(':thana', $thana);
    $stmt->bindParam(':latitude', $thana_lat_lng['latitude']);
    $stmt->bindParam(':longitude', $thana_lat_lng['longitude']);
    $stmt->bindParam(':link_builder', $link_builder);
    $stmt->execute();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <meta property="og:image" content="assets/img/points.png">
    <meta property="og:description" content="Ekhaneo Airtel">
    <meta property="og:title" content="Ekhaneo Airtel">

    <!--    [Site Title] -->
    <title>রেজিস্ট্রেশন | এখানেও এয়ারটেল </title>

    <!--    [Bootstrap Css] -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!--    [Animate Css]-->
    <link rel="stylesheet" href="assets/css/animate.css">

    <!--    [FontAwesome Css] -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <!--    [IcoFont Css] -->
    <link rel="stylesheet" href="assets/css/icofont.css">

    <!--    [OwlCarousel Css]-->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">

    <!--    [Custom Stlesheet]-->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">


    <!--    [Favicon] -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">

</head>

<body onload="getLatLng()">
    <!--PRELOADER START-->


    <!--    [ Strat Header Area]-->
    <header>
        <!--    [ Strat Logo Area]-->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="head-design">
                       <img src="assets/img/header.png" alt="">
                   </div>
                </div>
            </div>
        </div>
        <!--    [Finish Logo Area]-->
    </header>
    <!--    [Finish Header Area]-->

    <!--    [ Strat Section Title Area]-->
    <section id="add-location">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    
                     <form method="POST" action="signup.php" enctype="multipart/form-data">
                         
                         <input type="hidden" id="Latitude" name="geo_lat">
                        <input type="hidden" id="Longitude" name="geo_long">
                        
                    <div class="nam-pass  ">
                        <div class="row">
                            <div class="col-lg-4">
                                <h5>Name</h5>
                            </div>
                            <div class="col-lg-8"><input type="text" placeholder="Name" name="name"></div>
                        </div>
                    </div>
                    <div class="nam-pass">
                        <div class="row">
                            <div class="col-lg-4">
                                <h5>Phone</h5>
                            </div>
                            <div class="col-lg-8"><input type="number" placeholder="Phone" name="phone"></div>
                        </div>
                    </div>
                    <!-- Thana Place-->
                    <div class="nam-pass">
                        <div class="row">
                            <div class="col-lg-4">
                                <h5>Thana</h5>
                            </div>
                            <div class="col-lg-8">
                                <input list="thana" name="thana" placeholder="Thana Name">
                                    <datalist id="thana">
                                            <option value="Shibganj">
                                            <option value="Haripur">
                                            <option value="Baliadangi">
                                            <option value="Chapai Nawabganj Sadar">
                                            <option value="Ranisankail">
                                            <option value="Bholahat">
                                            <option value="Gomastapur">
											<option value="Nachole">
											<option value="Godagari">
											<option value="Thakurgaon Sadar">
											<option value="Tentulia">
											<option value="Pirganj">
											<option value="Atwari">
											<option value="Bochaganj">
											<option value="Niamatpur">
											<option value="Porsha">
											<option value="Panchagarh Sadar">
											<option value="Sapahar">
											<option value="Tanore">
											<option value="Biral">
											<option value="Paba">
											<option value="Boda">
											<option value="Kaharole">
											<option value="Rajpara">
											<option value="Birganj">
											<option value="Meherpur Sadar">
											<option value="Boalia">
											<option value="Mujib Nagar">
											<option value="Shah Makhdum">
											<option value="Dinajpur Sadar">
											<option value="Mohanpur">
											<option value="Patnitala">
											<option value="Matihar">
											<option value="Debiganj">
											<option value="Gangni">
											<option value="Manda">
											<option value="Damurhuda">
											<option value="Durgapur">
											<option value="Dhamoirhat">
											<option value="Charghat">
											<option value="Mahadebpur">
											<option value="Baghmara">
											<option value="Puthia">
											<option value="Khansama">
											<option value="Chirirbandar">
											<option value="Bagha">
											<option value="Maheshpur">
											<option value="Daulatpur">
											<option value="Nilphamari Sadar">
											<option value="Domar">
											<option value="Naogaon Sadar">
											<option value="Chuadanga Sadar">
											<option value="Alamdanga">
											<option value="Jiban Nagar">
											<option value="Parbatipur">
											<option value="Birampur">
											<option value="Bagati Para">
											<option value="Lalpur">
											<option value="Saidpur">
											<option value="Badalgachhi">
											<option value="Sharsha">
											<option value="Atrai">
											<option value="Fulbari">
											<option value="Jaldhaka">
											<option value="Chaugachha">
											<option value="Dimla">
											<option value="Natore Sadar">
											<option value="Mirpur">
											<option value="Raninagar">
											<option value="Patgram">
											<option value="Kalaroa">
											<option value="Satkhira Sadar">
											<option value="Bheramara">
											<option value="Taraganj">
											<option value="Kotchandpur">
											<option value="Debhata">
											<option value="Joypurhat Sadar">
											<option value="Badarganj">
											<option value="Kishoreganj">
											<option value="Adamdighi">
											<option value="Kushtia Sadar">
											<option value="Jhenaidah Sadar">
											<option value="Akkelpur">
											<option value="Hakimpur">
											<option value="Panchbibi">
											<option value="Kaliganj">
											<option value="Ishwardi">
											<option value="Harinakunda">
											<option value="Jhikargachha">
											<option value="Shyamnagar">
											<option value="Nawabganj">
											<option value="Singra">
											<option value="Kaliganj">
											<option value="Baraigram">
											<option value="Khetlal">
											<option value="Rangpur Sadar">
											<option value="Hatibandha">
											<option value="Dhupchanchia">
											<option value="Tala">
											<option value="Keshabpur">
											<option value="Mitha Pukur">
											<option value="Kotwali">
											<option value="Manirampur">
											<option value="Gangachara">
											<option value="Pabna Sadar">
											<option value="Assasuni">
											<option value="Kumarkhali">
											<option value="Ghoraghat">
											<option value="Shailkupa">
											<option value="Pirganj">
											<option value="Kalai">
											<option value="Gurudaspur">
											<option value="Nandigram">
											<option value="Atgharia">
											<option value="Kaliganj">
											<option value="Kahaloo">
											<option value="Gobindaganj">
											<option value="Shibganj">
											<option value="Chatmohar">
											<option value="Bagher Para">
											<option value="Paikgachha">
											<option value="Khoksa">
											<option value="Palashbari">
											<option value="Shalikha">
											<option value="Koyra">
											<option value="Bogra Sadar">
											<option value="Tarash">
											<option value="Dumuria">
											<option value="Aditmari">
											<option value="Magura Sadar">
											<option value="Shajahanpur">
											<option value="Pangsha">
											<option value="Pirgachha">
											<option value="Kaunia">
											<option value="Abhaynagar">
											<option value="Sadullapur">
											<option value="Sherpur">
											<option value="Bhangura">
											<option value="Sreepur">
											<option value="Narail Sadar">
											<option value="Lalmonirhat Sadar">
											<option value="Gabtali">
											<option value="Phultala">
											<option value="Sujanagar">
											<option value="Santhia">
											<option value="Kalukhali">
											<option value="Batiaghata">
											<option value="Sundarganj">
											<option value="Faridpur">
											<option value="Ullah Para">
											<option value="Royganj">
											<option value="Rajarhat">
											<option value="Sonatola">
											<option value="Balia Kandi">
											<option value="Gaibandha Sadar">
											<option value="Dacope">
											<option value="Khan Jahan Ali">
											<option value="Mohammadpur">
											<option value="Dhunat">
											<option value="Madhukhali">
											<option value="Phulbari">
											<option value="Dighalia">
											<option value="Khalishpur">
											<option value="Saghatta">
											<option value="Sariakandi">
											<option value="Kalia">
											<option value="Lohagara">
											<option value="Sonadanga">
											<option value="Khulna Sadar">
											<option value="Rajbari Sadar">
											<option value="Shahjadpur">
											<option value="Ulipur">
											<option value="Kazipur">
											<option value="Terokhada">
											<option value="Rupsa">
											<option value="Rampal">
											<option value="Fulchhari">
											<option value="Kurigram Sadar">
											<option value="Mongla">
											<option value="Bera">
											<option value="Sirajganj Sadar">
											<option value="Kamarkhanda">
											<option value="Boalmari">
											<option value="Bhurungamari">
											<option value="Fakirhat">
											<option value="Belkuchi">
											<option value="Alfadanga">
											<option value="Nageshwari">
											<option value="Bagerhat Sadar">
											<option value="Chilmari">
											<option value="Chauhali">
											<option value="Kashiani">
											<option value="Mollahat">
											<option value="Islampur">
											<option value="Madarganj">
											<option value="Daulatpur">
											<option value="Morrelganj">
											<option value="Gopalganj Sadar">
											<option value="Dewanganj">
											<option value="Saltha">
											<option value="Faridpur Sadar">
											<option value="Melandaha">
											<option value="Goalandaghat">
											<option value="Shibalaya">
											<option value="Sarishabari">
											<option value="Sarankhola">
											<option value="Char Rajibpur">
											<option value="Nagarpur">
											<option value="Raumari">
											<option value="Tangail Sadar">
											<option value="Muksudpur">
											<option value="Bhuapur">
											<option value="Gopalpur">
											<option value="Bakshiganj">
											<option value="Kalihati">
											<option value="Kachua">
											<option value="Tungi Para">
											<option value="Ghior">
											<option value="Nagarkanda">
											<option value="Chitalmari">
											<option value="Harirampur">
											<option value="Jamalpur Sadar">
											<option value="Dhanbari">
											<option value="Delduar">
											<option value="Zianagar">
											<option value="Mathbaria">
											<option value="Patharghata">
											<option value="Sreebardi">
											<option value="Nazirpur">
											<option value="Sherpur Sadar">
											<option value="Ghatail">
											<option value="Sadarpur">
											<option value="Char Bhadrasan">
											<option value="Bhanga">
											<option value="Bhandaria">
											<option value="Pirojpur Sadar">
											<option value="Saturia">
											<option value="Madhupur">
											<option value="Manikganj Sadar">
											<option value="Rajoir">
											<option value="Mirzapur">
											<option value="Kotali Para">
											<option value="Basail">
											<option value="Dhamrai">
											<option value="Jhenaigati">
											<option value="Barguna Sadar">
											<option value="Kawkhali">
											<option value="Bamna">
											<option value="Kanthalia">
											<option value="Nesarabad (Swarupkati)">
											<option value="Amtali">
											<option value="Nawabganj">
											<option value="Banari Para">
											<option value="Dohar">
											<option value="Wazirpur">
											<option value="Singair">
											<option value="Rajapur">
											<option value="Agailjhara">
											<option value="Madaripur Sadar">
											<option value="Shib Char">
											<option value="Sakhipur">
											<option value="Jhalokati Sadar">
											<option value="Kala Para">
											<option value="Nalitabari">
											<option value="Betagi">
											<option value="Nakla">
											<option value="Muktagachha">
											<option value="Gaurnadi">
											<option value="Kalkini">
											<option value="Kaliakair">
											<option value="Mirzaganj">
											<option value="Sreenagar">
											<option value="Savar">
											<option value="Fulbaria">
											<option value="Nalchity">
											<option value="Bakerganj">
											<option value="Mymensingh Sadar">
											<option value="Patuakhali Sadar">
											<option value="Keraniganj">
											<option value="Bhaluka">
											<option value="Lohajang">
											<option value="Zanjira">
											<option value="Phulpur">
											<option value="Haluaghat">
											<option value="Gazipur Sadar">
											<option value="Shariatpur Sadar">
											<option value="Babuganj">
											<option value="Serajdikhan">
											<option value="Barisal Sadar (Kotwali)">
											<option value="Muladi">
											<option value="Sreepur">
											<option value="Dumki">
											<option value="Darus Salams">
											<option value="Shah Ali">
											<option value="Hazaribagh">
											<option value="Mohammadpur">
											<option value="Naria">
											<option value="Adabor">
											<option value="Trishal">
											<option value="Galachipa">
											<option value="Mirpur">
											<option value="Pallabi">
											<option value="Lalbagh">
											<option value="Kamrangir Char">
											<option value="Sher-e-bangla Nagar">
											<option value="Turag">
											<option value="Dhanmondi">
											<option value="Damudya">
											<option value="Kafrul">
											<option value="New Market">
											<option value="Gosairhat">
											<option value="Kalabagan">
											<option value="Uttara">
											<option value="Tejgaon">
											<option value="Chak Bazar">
											<option value="Shahbagh">
											<option value="Cantonment">
											<option value="Ramna">
											<option value="Tejgaon Ind. Area">
											<option value="Bangshal">
											<option value="Gulshan">
											<option value="Biman Bandar">
											<option value="Kotwali">
											<option value="Dakshinkhan">
											<option value="Tongibari">
											<option value="Paltan">
											<option value="Sutrapur">
											<option value="Rampura">
											<option value="Motijheel">
											<option value="Uttar Khan">
											<option value="Khilkhet">
											<option value="Badda">
											<option value="Khilgaon">
											<option value="Gendaria">
											<option value="Shyampur">
											<option value="Jatrabari">
											<option value="Mehendiganj">
											<option value="Sabujbagh">
											<option value="Kadamtali">
											<option value="Bauphal">
											<option value="Bhedarganj">
											<option value="Narayanganj Sadar">
											<option value="Dhobaura">
											<option value="Hizla">
											<option value="Gaffargaon">
											<option value="Demra">
											<option value="Munshiganj Sadar">
											<option value="Kaliganj">
											<option value="Dashmina">
											<option value="Rupganj">
											<option value="Kapasia">
											<option value="Bandar">
											<option value="Ishwarganj">
											<option value="Sonargaon">
											<option value="Gauripur">
											<option value="Purbadhala">
											<option value="Bhola Sadar">
											<option value="Nandail">
											<option value="Chandpur Sadar">
											<option value="Gazaria">
											<option value="Palash">
											<option value="Matlab Uttar">
											<option value="Araihazar">
											<option value="Char Fasson">
											<option value="Hossainpur">
											<option value="Narsingdi Sadar">
											<option value="Durgapur">
											<option value="Manohardi">
											<option value="Pakundia">
											<option value="Haim Char">
											<option value="Meghna">
											<option value="Netrokona Sadar">
											<option value="Daulatkhan">
											<option value="Faridganj">
											<option value="Roypur">
											<option value="Burhanuddin">
											<option value="Lalmohan">
											<option value="Matlab Dakshin">
											<option value="Daudkandi">
											<option value="Kendua">
											<option value="Kishoreganj Sadar">
											<option value="Titas">
											<option value="Banchharampur">
											<option value="Lakshmipur Sadar">
											<option value="Homna">
											<option value="Hajiganj">
											<option value="Kalmakanda">
											<option value="Roypura">
											<option value="Kamalnagar">
											<option value="Ramganj">
											<option value="Barhatta">
											<option value="Kachua">
											<option value="Atpara">
											<option value="Tazumuddin">
											<option value="Belabo">
											<option value="Karimganj">
											<option value="Bajitpur">
											<option value="Tarail">
											<option value="Kuliar Char">
											<option value="Nabinagar">
											<option value="Muradnagar">
											<option value="Chandina">
											<option value="Ramgati">
											<option value="Nikli">
											<option value="Shahrasti">
											<option value="Madan">
											<option value="Noakhali Sadar (Sudharam)">
											<option value="Chatkhil">
											<option value="Manpura">
											<option value="Bhairab">
											<option value="Brahman Para">
											<option value="Itna">
											<option value="Dharampasha">
											<option value="Barura">
											<option value="Mohanganj">
											<option value="Ashuganj">
											<option value="Hatiya">
											<option value="Begumganj">
											<option value="Sonaimuri">
											<option value="Subarnachar">
											<option value="Manoharganj">
											<option value="Laksam">
											<option value="Brahmanbaria Sadar">
											<option value="Mithamain">
											<option value="Sarail">
											<option value="Austagram">
											<option value="Khaliajuri">
											<option value="Nasirnagar">
											<option value="Tahirpur">
											<option value="Comilla Adarsha Sadar">
											<option value="Kasba">
											<option value="Comilla Sadar Dakshin">
											<option value="Senbagh">
											<option value="Nangalkot">
											<option value="Akhaura">
											<option value="Kabirhat">
											<option value="Jamalganj">
											<option value="Companiganj">
											<option value="Bijoynagar">
											<option value="Ajmiriganj">
											<option value="Chauddagram">
											<option value="Lakhai">
											<option value="Baniachong">
											<option value="Bishwambarpur">
											<option value="Derai">
											<option value="Sulla">
											<option value="Daganbhuiyan">
											<option value="Madhabpur">
											<option value="Sunamganj Sadar">
											<option value="Sonagazi">
											<option value="Feni Sadar">
											<option value="Dakshin Sunamganj">
											<option value="Habiganj Sadar">
											<option value="Sandwip">
											<option value="Fulgazi">
											<option value="Parshuram">
											<option value="Nabiganj">
											<option value="Chunarughat">
											<option value="Chhagalnaiya">
											<option value="Mirsharai">
											<option value="Jagannathpur">
											<option value="Dowarabazar">
											<option value="Bahubal">
											<option value="Chhatak">
											<option value="Sitakunda">
											<option value="Sreemangal">
											<option value="Maulvi Bazar Sadar">
											<option value="Fatikchhari">
											<option value="Bishwanath">
											<option value="Balaganj">
											<option value="Matiranga">
											<option value="Companiganj">
											<option value="Hathazari">
											<option value="Pahartali">
											<option value="Halishahar">
											<option value="Chittagong Port">
											<option value="Patiya">
											<option value="Manikchhari">
											<option value="Khulshi">
											<option value="Sylhet Sadar">
											<option value="Kamalganj">
											<option value="Double Mooring">
											<option value="Dakshin Surma">
											<option value="Rajnagar">
											<option value="Bayejid Bostami">
											<option value="Panchlaish">
											<option value="Kotwali">
											<option value="Anowara">
											<option value="Bakalia">
											<option value="Chandgaon">
											<option value="Raozan">
											<option value="Kutubdia">
											<option value="Gowainghat">
											<option value="Maheshkhali">
											<option value="Banshkhali">
											<option value="Boalkhali">
											<option value="Panchhari">
											<option value="Pekua">
											<option value="Lakshmichhari">
											<option value="Khagrachhari Sadar">
											<option value="Fenchuganj">
											<option value="Kulaura">
											<option value="Chakaria">
											<option value="Golabganj">
											<option value="Satkania">
											<option value="Cox'S Bazar Sadar">
											<option value="Chandanaish">
											<option value="Kawkhali (Betbunia)">
											<option value="Rangunia">
											<option value="Mahalchhari">
											<option value="Ramu">
											<option value="Jaintiapur">
											<option value="Naniarchar">
											<option value="Dighinala">
											<option value="Lohagara">
											<option value="Ukhia">
											<option value="Lama">
											<option value="Kanaighat">
											<option value="Juri">
											<option value="Beani Bazar">
											<option value="Rangamati Sadar">
											<option value="Barlekha">
											<option value="Langadu">
											<option value="Teknaf">
											<option value="Rajasthali">
											<option value="Baghai Chhari">
											<option value="Bandarban Sadar">
											<option value="Naikhongchhari">
											<option value="Zakiganj">
											<option value="Alikadam">
											<option value="Barkal">
											<option value="Rowangchhari">
											<option value="Belai Chhari">
											<option value="Thanchi">
											<option value="Ruma">
                                </datalist>
                            </div>
                        </div>
                    </div>
                    
                    <div class="check-btn">
                        <button type="submit" name="submit"><img src="assets/img/point.png" alt=""> Check In</button>
                    </div>
                    
                    <!-- [FORM END] --> 
                    </form>
                    
                </div>
            </div>
        </div>
    </section>
    <!--    [Finish Section Title Area]-->


    <!--    [ Strat Section Area]-->
    <!--    [Finish Section Area]-->


    <!--    [ Start Footer Area]-->
    <footer>
        <div class="copy-right">
            <div class="container">
               <div class="copy-content">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <p>&copy; 2018. All Rights Reserved. Theme By <span>PreneurLAB.com</span></p>
                        </div>
                    </div>
               </div>
            </div>
        </div>
    </footer>
    <!--    [Finish Footer Area]-->

    <!--SCROLL TOP BUTTON-->
    <a href="#" class="top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <!--    [jQuery]-->
    <script src="assets/js/jquery-3.2.1.min.js"></script>

    <!--    [Popper Js] -->
    <script src="assets/js/popper.min.js"></script>

    <!--    [Bootstrap Js] -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!--    [OwlCarousel Js]-->
    <script src="assets/js/owl.carousel.min.js"></script>

    <!--    [Navbar Fixed Js] -->
    <script src="assets/js/navbar-fixed.js"></script>

    <!--    [Main Custom Js] -->
    <script src="assets/js/main.js"></script>
    
    <script>
        function getLatLng(){
            
            var options = {
              enableHighAccuracy: true,
              timeout: 5000,
              maximumAge: 0
            };
            
            function success(pos) {
              var crd = pos.coords;
                
              document.getElementById('Latitude').value = crd.latitude;
              document.getElementById('Longitude').value = crd.longitude;
            };
            
            function error(err) {
              console.warn(`ERROR(${err.code}): ${err.message}`);
            };
            
            navigator.geolocation.getCurrentPosition(success, error, options);
        }
    </script>
</body>

</html>
